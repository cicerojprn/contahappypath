package banco;

import banco.Conta;
import banco.OperacaoIlegalException;
import junit.framework.TestCase;

public class ContaTest extends TestCase {

	public void testDebitarSucesso() throws OperacaoIlegalException {
		Conta c = new Conta("123", 20);
		c.debitar(10);
		assertEquals(10, c.getSaldo(), 0.0);
	}

	// slides 5 e 6
	public void testCreditarSucesso() throws OperacaoIlegalException {
		Conta c = new Conta("123", 20);
		c.creditar(10);

		// slide 5
		// assertEquals( 10, c.getSaldo(), 0.0);

		// slide 6
		assertEquals(30, c.getSaldo(), 0.0);
	}

	// slides 8, 9, 10, 11
	public void testTransferirSucesso() throws OperacaoIlegalException {
		Conta origem = new Conta("123", 30);
		Conta destino = new Conta("321", 0);
		origem.transferir(destino, 10);
		assertEquals(20, origem.getSaldo(), 0.0);
		assertEquals(10, destino.getSaldo(), 0.0);
	}
}
