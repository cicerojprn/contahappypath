package banco;

public class Conta {

	private String codigo;
	private double saldo = 0;

	public Conta(String p_id, double p_saldo) {
		this.saldo = p_saldo;
		this.codigo = p_id;
	}

	public double getSaldo() {
		return saldo;
	}

	public String getCodigo() {
		return codigo;
	}

	public double debitar(double valor) throws OperacaoIlegalException {
		if (valor > 0 && saldo >= valor) {
			saldo = saldo - valor;
		} else {
			throw new OperacaoIlegalException();
		}
		return saldo;
	}

	public double creditar(double valor) throws OperacaoIlegalException {
		if (valor > 0) {
			saldo = saldo + valor;
		} else {
			throw new OperacaoIlegalException();
		}
		return saldo;
	}

	public double transferir(Conta destino, double valor) throws OperacaoIlegalException {
		if (destino != null && !this.equals(destino)) {
			debitar(valor);
			destino.creditar(valor);
		}else{
			throw new OperacaoIlegalException();
		}
		return this.saldo;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Conta) {
			Conta contaTemp = (Conta) obj;
			if (codigo == contaTemp.getCodigo()) {
				return true;
			}
		} else {
			return false;
		}
		return super.equals(obj);
	}
}
